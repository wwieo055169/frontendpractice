from flask import Flask, request, url_for, redirect, render_template, Blueprint
from views.post_route import post_route
from . import home

@home.route('/', methods=["GET", "POST"])
def homepage():
    if request.method == "POST":
        return post_route()      
    return render_template("home/home.html")

@home.route('/account', methods=["GET", "POST"])
def account():
    if request.method == "POST":
        return post_route()      
    return render_template("home/account.html")

@home.route('/forum', methods=["GET", "POST"])
def forum():
    if request.method == "POST":
        return post_route()      
    return render_template("home/forum.html")

@home.route('/knowledge_link', methods=["GET", "POST"])
def knowledge_link():
    if request.method == "POST":
        return post_route()      
    return render_template("home/knowledge_link.html")

@home.route('/room_set', methods=["GET", "POST"])
def room_set():
    if request.method == "POST":
        return post_route()      
    return render_template("home/room_set.html")

@home.route('/source', methods=["GET", "POST"])
def source():
    if request.method == "POST":
        return post_route()      
    return render_template("home/source.html")

@home.route('/update_notes', methods=["GET", "POST"])
def update_notes():
    if request.method == "POST":
        return post_route()      
    return render_template("home/update_notes.html")