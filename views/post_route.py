from flask import Flask, request, url_for, redirect, render_template

def post_route():
    if request.method == "POST":
        route = request.form["submit_button"]
        return redirect(url_for(route))