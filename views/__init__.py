from flask import Blueprint

home = Blueprint('home', __name__)
forum = Blueprint('forum', __name__)
source = Blueprint('source', __name__)
account = Blueprint('account', __name__)
room_set = Blueprint('room_set', __name__)

from .home import home
from .forum import forum
from .source import source
from .account import account
from .room_set import room_set

from .post_route import post_route