from flask import Flask, request, url_for, redirect, render_template, Blueprint
from views.post_route import post_route
from . import room_set

@room_set.route('/bystander', methods=["GET", "POST"])
def bystander():
    if request.method == "POST":
        return post_route()      
    return render_template("room_set/bystander.html")

@room_set.route('/choose_model', methods=["GET", "POST"])
def choose_model():
    if request.method == "POST":
        return post_route()      
    return render_template("room_set/choose_model.html")

@room_set.route('/end', methods=["GET", "POST"])
def end():
    if request.method == "POST":
        return post_route()      
    return render_template("room_set/end.html")

@room_set.route('/playing', methods=["GET", "POST"])
def playing():
    if request.method == "POST":
        return post_route()      
    return render_template("room_set/playing.html")