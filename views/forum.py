from flask import Flask, request, url_for, redirect, render_template, Blueprint
from views.post_route import post_route
from . import forum

@forum.route('/play_together', methods=["GET", "POST"])
def play_together():
    if request.method == "POST":
        return post_route()      
    return render_template("forum/play_together.html")

@forum.route('/post_article', methods=["GET", "POST"])
def post_article():
    if request.method == "POST":
        return post_route()      
    return render_template("forum/post_article.html")

@forum.route('/report', methods=["GET", "POST"])
def report():
    if request.method == "POST":
        return post_route()      
    return render_template("forum/report.html")

@forum.route('/script', methods=["GET", "POST"])
def script():
    if request.method == "POST":
        return post_route()      
    return render_template("forum/script.html")

@forum.route('/share_source', methods=["GET", "POST"])
def share_source():
    if request.method == "POST":
        return post_route()      
    return render_template("forum/share_source.html")