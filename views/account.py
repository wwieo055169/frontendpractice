from flask import Flask, request, url_for, redirect, render_template, Blueprint
from views.post_route import post_route
from . import account

@account.route('/achievement', methods=["GET", "POST"])
def achievement():
    if request.method == "POST":
        return post_route()      
    return render_template("account/achievement.html")

@account.route('/login_page', methods=["GET", "POST"])
def login_page():
    if request.method == "POST":
        return post_route()      
    return render_template("account/login_page.html")

@account.route('/personal_source', methods=["GET", "POST"])
def personal_source():
    if request.method == "POST":
        return post_route()      
    return render_template("account/personal_source.html")

@account.route('/play_record', methods=["GET", "POST"])
def play_record():
    if request.method == "POST":
        return post_route()      
    return render_template("account/play_record.html")

@account.route('/setting', methods=["GET", "POST"])
def setting():
    if request.method == "POST":
        return post_route()      
    return render_template("account/setting.html")