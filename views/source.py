from flask import Flask, request, url_for, redirect, render_template, Blueprint
from views.post_route import post_route
from . import source

@source.route('/background', methods=["GET", "POST"])
def background():
    if request.method == "POST":
        return post_route()      
    return render_template("source/background.html")

@source.route('/object', methods=["GET", "POST"])
def object():
    if request.method == "POST":
        return post_route()      
    return render_template("source/object.html")

@source.route('/role', methods=["GET", "POST"])
def role():
    if request.method == "POST":
        return post_route()      
    return render_template("source/role.html")
