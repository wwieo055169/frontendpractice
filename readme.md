# Frontend web

## Virtual Environment Install
```sh
$ pip3 install virtualenv
```

## Virtual Environment
```sh
$ virtualenv venv
```

## Enter Environment
```sh
$  . venv/bin/activate
```

# Package installing
## For production
```sh
(venv)$ pip3 install -r requirements.txt
```
