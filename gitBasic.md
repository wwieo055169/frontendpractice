# Git

## General

### git init
Init a git repo
```sh
$ git init .
```

### git status
Status current git.  
This is your best friend to know what's going on.  
```sh
$ git status
On branch master
Your branch is up to date with 'origin/master'.
```

### git log
Shows every commit.  
```sh
# This shows detail commit message
$ git log

# Show with only commit title
$ git log --oneline

# Show with all branch graph and colored it.
$ git log --graph --online --all --decorate
```
  
### git add
Add files into stage.  
```sh
# This add all changed file inside current directory
$ git add .

# Or you want just some file
$ git add this_is_some_file.py

# Even with wildcard.
$ git add *.py
```
[Reference](https://backlog.com/git-tutorial/tw/intro/intro2_4.html)  
  
### git rm
Remove file from stage AND LOCAL!!!!  
Running this command SHOULD NEVER WITHOUT `--cached`.  
```sh
# Remove all staged file but not delete them.
$ git rm . --cached

# Just some file from stage.
$ git rm aaa.py --cached

# With wildcard.
$ git rm *.py --cached
```
[Reference](https://gitbook.tw/chapters/using-git/rename-and-delete-file.html)  
  
### git commit
Commit your changes from stage to storage.  
```sh
$ git commit
..... Write your commit message .....
```
  
About how to write a commit message  
1. Separate subject from body with a blank line  
2. Limit the subject line to 50 characters  
3. Capitalize the subject line  
4. Do not end the subject line with a period  
5. Use the imperative mood in the subject line  
6. Wrap the body at 72 characters  
7. Use the body to explain what and why vs. how  
8. WRITE IN ENGLISH!!!!!!!!!!!!  

[Reference](https://blog.louie.lu/2017/03/21/%E5%A6%82%E4%BD%95%E5%AF%AB%E4%B8%80%E5%80%8B-git-commit-message/)  
  
### git push
This push all your commits to remote for sync.  
!!!!!!! Never push with `--force` argument, or someone will kill you (maybe) !!!!!!!  
```sh
$ git push [remote_name] [local_branch]:[remote_branch]

# These two is same.
$ git push origin master:master
$ git push origin master
```
[Reference](https://gitbook.tw/chapters/github/push-to-github.html)

### git pull
This pulls from remote branch.
```sh
$ git pull [remote_name] [branch_name]

Pull from remote branch and merge with current changes.
$ git pull --rebase [remote_name] [branch_name]
```
[Reference](https://gitbook.tw/chapters/github/pull-from-github.html)  

## Branch

### git branch
This create and rm a branch
```sh
# Duplicate current branch with name
$ git branch [branch name]

# Remove a merged branch
$ git branch -d [branch name]
# Force remove a branch whether merged or not
$ git branch -D [branch name]
```
[Reference](https://git-scm.com/book/zh-tw/v1/Git-%E5%88%86%E6%94%AF-%E5%88%86%E6%94%AF%E7%9A%84%E6%96%B0%E5%A2%9E%E8%88%87%E5%90%88%E4%BD%B5)  

### git checkout
This switch to specific branch
```sh
$ git checkout [branch name]

# Duplicate current branch with name
$ git git checkout -b [branch name]
```
[Reference](https://backlog.com/git-tutorial/tw/stepup/stepup2_3.html)  

### git merge
This merges B branch to A branch
```sh
$ git checkout [branch_A]
$ git merge [branch_B]
```
[Reference](https://git-scm.com/book/zh-tw/v2/%E4%BD%BF%E7%94%A8-Git-%E5%88%86%E6%94%AF-%E5%88%86%E6%94%AF%E5%92%8C%E5%90%88%E4%BD%B5%E7%9A%84%E5%9F%BA%E6%9C%AC%E7%94%A8%E6%B3%95)  

### git reset
Reset current directory's content
```sh
# Back to commit A
$ git reset [commit A''s hash]

# One commit before HEAD
$ git reset HEAD~1

# Two commit before HEAD
$ git reset HEAD~2

# Hard argument will reset all content in current folder.
$ git reset --hard [commit hash]

# Soft argument will not reset contents in current folder but only git history.
$ git reset --soft [commit hash]
```
[Reference](https://blog.wu-boy.com/2010/08/git-%E7%89%88%E6%9C%AC%E6%8E%A7%E5%88%B6%EF%BC%9A%E5%88%A9%E7%94%A8-git-reset-%E6%81%A2%E5%BE%A9%E6%AA%94%E6%A1%88%E3%80%81%E6%9A%AB%E5%AD%98%E7%8B%80%E6%85%8B%E3%80%81commit-%E8%A8%8A%E6%81%AF/)  

testtest
