from flask import Flask, request
from views.home import home
from views.forum import forum
from views.source import source
from views.account import account
from views.room_set import room_set

app = Flask('__name__')
app.register_blueprint(home)
app.register_blueprint(forum, url_prefix="/forum")
app.register_blueprint(source, url_prefix="/source")
app.register_blueprint(account, url_prefix="/account")
app.register_blueprint(room_set, url_prefix="/room_set")

if __name__ == "__main__":
    app.run()